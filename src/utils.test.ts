import { expect, test } from "bun:test"
import { DateTime } from "luxon"
import { timeToString } from "./utils"

test("time to string", () => {
  expect(timeToString("2021-01-02T03:04:05+0600")).toEqual(
    "2021-01-02T03:04:05+0600",
  )
  expect(
    timeToString(
      DateTime.fromISO("2021-01-02T03:04:05+0600", { setZone: true }),
    ),
  ).toEqual("2021-01-02T03:04:05+0600")
  expect(
    timeToString(
      DateTime.fromISO("2021-01-02T03:04:05+0600").toJSDate(),
      "+06",
    ),
  ).toEqual("2021-01-02T03:04:05+0600")
})
