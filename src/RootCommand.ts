import { Command, Help } from "commander"

// Subcommands are still instances of the original Command class, so they will
// not get this overriden behavior. This is probably why netlify-cli defines a
// new command class for each subcommand.
//
// Here, this behavior happens to be what I want, as I only want to override the
// help text in the root command. (If we used `configureHelp` instead, that
// would apply to subcommands as well.)

const groupedCommands: Record<string, Array<string | [string, string]>> = {
  Config: [
    ["config", "see all configuration keys"],
    ["config <key>", "read value of configuration <key>"],
    ["config <key> <value>", "set configuration <key> to <value>"],
  ],
  Info: ["status", "task <task>", "moment <moment>", "list-tasks"],
  Write: ["switch", "clock-out"],
  Edit: ["sync", "visit"],
  Help: ["about", ["help [command]", "display help for command"]],
}

/**
 * Given a spec like "abc" or "def <opt>", return the first word for matching.
 */
function groupedCommandName(spec: string | [string, string]) {
  if (typeof spec === "string") return spec.split(" ")[0]
  return spec[0].split(" ")[0]
}

class MyHelp extends Help {
  // This turns off the built-in "commands" section, except, when in dev mode, commands that I
  // forgot to document and add to the list above.
  visibleCommands(cmd: Command) {
    // NODE_ENV is a static global, so the variable and its surrounding
    // expression would be evaluated at build time.
    //
    // Writing this this way allows the "documentedCommands" declaration to not
    // even make its way into the compiled code.
    return process.env.NODE_ENV === "development"
      ? (() => {
          const documentedCommands: Set<string> = new Set(
            Object.values(groupedCommands)
              .flat(1)
              .map((elem) => groupedCommandName(elem)),
          )
          return super
            .visibleCommands(cmd)
            .filter((sub) => !documentedCommands.has(sub.name()))
        })()
      : []
  }
  formatHelp(cmd: Command, helper: Help) {
    // We need to copy these from the original to get correct wrapping
    // behavior, unfortunately.
    const helpWidth = helper.helpWidth || 80
    const itemIndentWidth = 2
    const itemSeparatorWidth = 2
    function formatItem(
      term: string,
      description: string,
      maxTermWidth: number,
    ) {
      if (description) {
        const fullText = `${term.padEnd(maxTermWidth + itemSeparatorWidth)}${description}`
        return helper.wrap(
          fullText,
          helpWidth - itemIndentWidth,
          maxTermWidth + itemSeparatorWidth,
        )
      }
      return term
    }

    const output = [super.formatHelp(cmd, helper)]
    /**
     * Mapping from sub command names to their Commander descriptions, to be
     * used as a default when I haven't specified another description above.
     */
    const cmdNameToRegisteredDesc = new Map(
      cmd.commands.map((sub) => [
        sub.name(),
        helper.subcommandDescription(sub),
      ]),
    )
    for (const [group, commands] of Object.entries(groupedCommands)) {
      const maxTermWidth = commands
        .map((cmd) => (typeof cmd === "string" ? cmd : cmd[0]))
        .reduce((max, cmd) => {
          return Math.max(max, cmd.length)
        }, 0)
      output.push(`${group} commands:`)
      for (const it of commands) {
        const isStr = typeof it === "string"
        const cmdname = isStr ? it : it[0]
        const desc = isStr
          ? cmdNameToRegisteredDesc.get(groupedCommandName(it)) || ""
          : it[1]
        output.push(
          formatItem(cmdname, desc, maxTermWidth).replace(
            /^/gm,
            " ".repeat(itemIndentWidth),
          ),
        )
      }
      output.push("")
    }
    return output.join("\n")
  }
}

export class RootCommand extends Command {
  createHelp() {
    return Object.assign(new MyHelp(), this.configureHelp())
  }
}
