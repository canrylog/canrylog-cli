import { expect, test } from "bun:test"
import { exists } from "node:fs/promises"
import { DateTime } from "luxon"
import { trackingFile } from "./config"
import * as file from "./file"

test("event parsing", () => {
  expect(file.parseEvent("2020-01-02T03:04:05+0900  task name  abc")).toEqual(
    new file.Event("2020-01-02T03:04:05+0900", "task name  abc"),
  )
})

test("start from empty", async () => {
  expect(await file.GetTaskNames()).toEqual(new Set([]))
})

test("file now exists", async () => {
  expect(await exists(trackingFile())).toBe(true)
})

test("can switch task", async () => {
  expect(await file.SwitchTask("New task")).toBe(true)
})

test("task is now switched", async () => {
  const currentTask = await file.GetCurrentTask()
  expect(currentTask).toEqual("New task")
})

test("can switch more tasks", async () => {
  expect(await file.SwitchTask("Another task")).toBe(true)
  expect(await file.SwitchTask("Third task")).toBe(true)
})

test("task list", async () => {
  const tasks = await file.GetTaskNames()
  expect(tasks).toEqual(new Set(["New task", "Another task", "Third task"]))
})

test("task duration", async () => {
  const { duration, lastActiveTimestamp } =
    await file.GetTaskDuration("New task")
  expect(duration).toBe(0)
  expect(DateTime.fromISO(lastActiveTimestamp).toUnixInteger()).toBeTruthy()
})
