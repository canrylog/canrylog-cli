import Enquirer from "enquirer"
import { GetTaskNames } from "./file"

/**
 * Ask the user to select a task from all tasks.
 */
export async function ReadTask(prompt: string): Promise<string | undefined> {
  try {
    const response = (await Enquirer.prompt([
      {
        type: "autocomplete",
        name: "task",
        message: prompt || "Task",
        // @ts-ignore upstream declaration bug
        limit: 10,
        choices: [...(await GetTaskNames())],
      },
    ])) as { task: string | undefined }
    return response?.task
  } catch (_e) {
    throw new Error("<read.ts> Cancelled by user")
  }
}
