import { expect, test } from "bun:test"
import { homedir } from "node:os"
import { join } from "node:path"
import * as config from "./config"

test("repo path", () => {
  expect(config.config().get("path.repo")).toEqual(join(homedir(), ".canrylog"))
  test("config() returns the same object", () => {
    expect(config.config()).toBe(config.config())
  })
})

test("tracking file", async () => {
  const filePath = join(homedir(), ".canrylog", "tracking.canrylog")
  expect(config.trackingFile()).toEqual(filePath)
})
