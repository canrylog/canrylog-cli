/**
 * @file Markdown string constants.
 *
 * Marked + marked-terminal is really big (like 2 MB, which is the same size as
 * everything else at the time of writing).
 *
 * To avoid needing Marked at runtime, this file converts Markdown strings into
 * chalk-template strings, so that Marked can be run at compile time as a Bun
 * macro.
 */

import { Marked } from "marked"
import type { MarkedExtension } from "marked"
import { markedTerminal } from "marked-terminal"

const marked = new Marked(
  markedTerminal({
    code: (...xs: string[]) => `{yellow ${xs.join("")}}`,
    blockquote: (...xs: string[]) => `{gray.italic ${xs.join("")}}`,
    html: (...xs: string[]) => `{gray ${xs.join("")}}`,
    heading: (...xs: string[]) => `{green.bold ${xs.join("")}}`,
    firstHeading: (...xs: string[]) =>
      `{magenta.underline.bold ${xs.join("")}}`,
    hr: (...xs: string[]) => `{reset ${xs.join("")}}`,
    listitem: (...xs: string[]) => `{reset ${xs.join("")}}`,
    table: (...xs: string[]) => `{reset ${xs.join("")}}`,
    paragraph: (...xs: string[]) => `{reset ${xs.join("")}}`,
    strong: (...xs: string[]) => `{bold ${xs.join("")}}`,
    em: (...xs: string[]) => `{italic ${xs.join("")}}`,
    codespan: (...xs: string[]) => `{yellow ${xs.join("")}}`,
    del: (...xs: string[]) => `{dim.gray.strikethrough ${xs.join("")}}`,
    link: (...xs: string[]) => `{blue ${xs.join("")}}`,
    href: (...xs: string[]) => `{blue.underline ${xs.join("")}}`,
  }) as MarkedExtension,
)

export const errVisitNoEditor = () =>
  marked.parse(`No editor configured.

Please set it via the \`$EDITOR\` or \`$VISUAL\` environment variables, like so:

\`\`\`sh
export EDITOR=nvim
\`\`\`

The tracking file location is: **$1**`) as string
