import { existsSync } from "node:fs"
import simpleGit, { type SimpleGit } from "simple-git"
import { config } from "./config.ts"

/**
 * Return the repository containing the tracking file.
 */
function trackingRepo(): string {
  const repo = config().get("path.repo")
  if (typeof repo !== "string") {
    throw new Error("Configuration file is not valid")
  }
  return repo
}

let _repo: SimpleGit | undefined
function getRepoObj() {
  if (typeof _repo !== "undefined") return _repo
  if (!existsSync(trackingRepo())) return undefined
  _repo = simpleGit({ baseDir: trackingRepo() })
  return _repo
}

/**
 * Return whether the repository should be synchronized.
 * If synchronization should happen, the repository object is returned for
 * convenience.
 */
export async function repoShouldSync() {
  const repoExists = existsSync(trackingRepo())
  if (!repoExists || !config().get("sync.enabled")) {
    return
  }
  const repo = getRepoObj()
  if (!repo || !(await repo.checkIsRepo())) {
    return
  }

  return repo
}
