import { DateTime } from "luxon"
import type { Zone } from "luxon"
import { Err, Ok } from "ts-results-es"

/**
 * Wrap a promise as a Result instead of allowing it to throw.
 */
export function promiseResult<T>(promise: Promise<T>) {
  return promise.then((v) => Ok(v)).catch((e) => Err(e as Error))
}

/**
 * Run `fn` and return its value.
 * If `fn` throws, exit. Do not show backtrace or print source line.
 */
export function tryOrExit<T>(fn: () => T): T {
  try {
    return fn()
  } catch (e) {
    if (e instanceof Error) {
      console.log(e.message)
    }
    process.exit(1)
  }
}

/**
 * Convert `moment` to a string.
 * If `moment` is a native Date, the timezone should be provided via `zone`.
 */
export function timeToString(
  moment: string | DateTime | Date | undefined,
  zone?: string | Zone<true>,
) {
  // passthrough
  if (typeof moment === "string") return moment
  const now =
    moment instanceof Date
      ? DateTime.fromJSDate(moment, { zone }) // cast to DateTime if it's native Date
      : moment instanceof DateTime
        ? moment // pass through if it's already DateTime
        : DateTime.now() // otherwise take "now"
  return now.toFormat("yyyy-MM-dd'T'HH:mm:ssZZZ")
}
