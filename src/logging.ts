import chalk from "chalk"
import { template as chalkTemplate } from "chalk-template"

export function substituteChalkTemplate(s: string) {
  return chalkTemplate(s).trim()
}

export function substituteDefList(s: string) {
  return s.replace(
    /- (.+) (::)/gu,
    (_match, p1: string, p2: string) =>
      `- ${p1
        .split(",")
        .map((it) => {
          const item = it.trim()
          return chalk.bold(
            item.replace(/\[([^ ]+)\]\(([^ ]+)\)/, `\$1 (${chalk.blue("$2")})`),
          )
        })
        .join(", ")} ${chalk.blue(p2)}`,
  )
}
