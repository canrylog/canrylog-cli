#!/usr/bin/env node
import { writeSync } from "node:fs"

// import { Command } from "@commander-js/extra-typings";
import inquirer from "inquirer"
import DatePrompt from "inquirer-date-prompt"
import { DateTime } from "luxon"
import openEditor from "open-editor"
import { RootCommand } from "./RootCommand.ts"
import { substituteChalkTemplate, substituteDefList } from "./logging.ts"
import { errVisitNoEditor } from "./markdownStrings.ts" with { type: "macro" }

// @ts-ignore
inquirer.registerPrompt("date", DatePrompt)

import pkg from "../package.json"
import {
  cmdConfig,
  cmdSwitchTask,
  cmdSyncPullRebase,
  cmdSyncPush,
  cmdTaskInfo,
} from "./commands"
import { trackingFile } from "./config"
import { GetCurrentTask, GetMomentTask, GetTaskNames } from "./file"
import { timeToString } from "./utils"

// The default behavior on an uncaught exception is to print the source line
// with the error, then print the backtrace or message. Since we ship minified
// code, the "source" line is actually minified, and can easily take up half the
// window. Override the behavior to not do that.
//
// The backtrace is still kept since we should still be catching errors and
// showing our own error messages.
process.on("uncaughtException", (err) => {
  if (err.message === "<read.ts> Cancelled by user") {
    return
  }
  writeSync(process.stderr.fd, err.stack || err.message)
  process.exitCode = 1
})

// We use a subclass instead of `configureHelp` because that allows us to only
// override some of the help text for the root command.
export const program = new RootCommand()
program
  .name(pkg.name)
  .summary(pkg.description)
  .description(
    `A personal time tracker.
Tasks are tracked by logging events into the tracking file.`,
  )
  .version(pkg.version)

// TODO: interactive configuration
program
  .command("config")
  .summary("configure canrylog")
  .description(
    `Configure Canrylog.
When not given a key or given a nonexistant key, all valid keys will be shown.
When given a valid key and value, the new value will be set.`,
  )
  .argument("[key]", "the option to set")
  .argument("[value]", "the new value")
  .action(cmdConfig)

program
  .command("switch")
  .summary("switch to another task")
  .description(
    `Switch to another task.

Prompts to select from existing tasks if no <task> is given.`,
  )
  .argument("[task]", "the task to switch to")
  .action(cmdSwitchTask)

program
  .command("clock-out")
  .summary("clock out of the current task")
  .description(`Clock out by switching the current task to "Downtime".`)
  .action(() => {
    cmdSwitchTask("Downtime")
  })

program
  .command("list-tasks")
  .description("list all available tasks")
  .action(async () => {
    for (const name of [...(await GetTaskNames())].reverse()) {
      console.log(name)
    }
  })

program
  .command("status")
  .summary("show current status")
  .action(async () => {
    console.log(`Current task: ${await GetCurrentTask()}`)
  })

const syncSub = program
  .command("sync")
  .summary("tracking file synchronization")
  .description(
    `Synchronization of the tracking file.
This currently relies on Git, and is honestly questionable security-wise.`,
  )
  .action(async () => {
    // evolving any
    let result
    result = await cmdSyncPullRebase(true)
    if (result.isErr()) {
      if (result.error === "should not sync") {
        console.log("Sync is disabled")
      } else {
        console.log(result.error)
      }
      return
    }
    result = await cmdSyncPush()
    if (result.isErr()) {
      if (result.error === "should not sync") {
        console.log("Sync is disabled")
      } else {
        console.log(result.error.message)
      }
      return
    }
  })
syncSub
  .command("pull")
  .summary("fetch from remote then apply changes on top")
  .description(
    `Fetch from remote, then apply changes on top.
This does git fetch && git rebase, in order to produce a more linear commit
history in the tracking repository. That repository is still going to be
inefficient, but for now this will do.`,
  )
  .action(() => {
    cmdSyncPullRebase()
  })
syncSub
  .command("push")
  .summary("push new changes")
  .action(() => {
    cmdSyncPush()
  })

program
  .command("visit")
  .summary("open the tracking file in your editor")
  .action(async () => {
    try {
      await openEditor([trackingFile()])
    } catch (e) {
      if (
        e instanceof Error &&
        e.message
          .trim()
          .startsWith("Your $EDITOR environment variable is not set.")
      ) {
        console.log(
          substituteChalkTemplate(
            errVisitNoEditor().replace("$1", trackingFile()),
          ),
        )
      }
    }
  })

program
  .command("moment")
  .summary("describe a moment")
  .action(async () => {
    const { timestamp } = await inquirer.prompt({
      type: "date",
      name: "timestamp",
      message: "For which moment?",
    })
    const datetime = DateTime.fromJSDate(timestamp)
    console.log(
      `The task active at ${timeToString(datetime)} was ${await GetMomentTask(
        datetime,
      )}.`,
    )
  })

program
  .command("task")
  .summary("describe a task")
  .description("Describe a task's information.")
  .argument("[task]", "the task to show information for")
  .action(cmdTaskInfo)

program
  .command("about")
  .summary("about canrylog")
  .action(() => {
    console.log(
      substituteDefList(`Canrylog is another time tracker.

It's made for tracking anything manually, based on logging events into a
tracking file.

Canrylog depends on these projects:

- [Commander](https://github.com/tj/commander.js) ::
  the command line framework Canrylog uses
- [Conf](https://github.com/sindresorhus/conf) ::
  relied on for configuration management
- [Enquirer](https://github.com/enquirer/enquirer) ::
  for autocomplete prompts
- [inquirer-date-prompt](https://github.com/haversnail/inquirer-date-prompt) ::
  for datetime prompts
- [@codemirror/state](https://codemirror.net/docs/ref/#state) ::
  does the heavy lifting of providing a framework for reading and seeking in a
  text file
- [simple-git](https://github.com/steveukx/git-js) ::
  for basic synchronization in \`canrylog sync\`
- [open-editor](https://github.com/sindresorhus/open-editor) ::
  for \`canrylog visit\`
- [luxon](https://moment.github.io/luxon/) ::
  for timestamp parsing and formatting, and datetime processing
- [iterator-helper](https://github.com/alkihis/iterator-helper) ::
  polyfill for iterator helpers, which is currently a little too new
- [ts-results-es](https://github.com/lune-climate/ts-results-es) ::
  For errors that can be typed and returned instead of thrown
- [TypeScript](https://www.typescriptlang.org/) ::
  for typechecking during development
- [Biome](https://biomejs.dev) ::
  for linting and formatting during development
- [Bun](https://bun.sh) ::
  Used as the package manager and bundler
- [Node](https://nodejs.org) ::
  The good ol' reliable foundational JS runtime
`),
    )
  })

program.parseAsync()
