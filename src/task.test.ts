import { expect, test } from "bun:test"

import * as task from "./task"

test("taskAncestors", () => {
  expect(task.taskAncestors("a:b:c")).toEqual(["a:b", "a"])
})

test("taskBasename", () => {
  expect(task.taskBasename("Task1")).toEqual("Task1")
})
