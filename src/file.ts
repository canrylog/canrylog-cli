/**
 * Functions that directly work with the tracking file.
 */

import { appendFile, mkdir, readFile, writeFile, open } from "node:fs/promises"
import { EditorState } from "@codemirror/state"
import type { Text } from "@codemirror/state"
import { iter } from "iterator-helper"
import type { HIterator } from "iterator-helper"
import { DateTime } from "luxon"

import { dirname } from "node:path"
import { Ok } from "ts-results-es"
import { trackingFile } from "./config"
import { taskAncestors } from "./task"
import { timeToString } from "./utils"

const eventMatchRegex = /([^\n]+?)  ([^\n]+)/

let fileString: string | undefined
let editor: EditorState | undefined
let dirty = false

export class Event<T> {
  timestamp: T
  task: T
  constructor(timestamp: T, task: T) {
    this.timestamp = timestamp
    this.task = task
  }
}

/** Parse a string into an Event. */
export function parseEvent(line: string) {
  const match = line.match(eventMatchRegex)
  if (!match) {
    return new Event(null, null)
  }
  return new Event(match[1], match[2])
}

/** Return the event within `doc` at `line`. */
function eventAt(doc: Text, line: number) {
  return parseEvent(doc.line(line).text)
}

/**
 * Return the moment within `doc` at `line`, or `now`, or DateTime.now().
 * `now` is for synchronizing the current time over multiple invocations.
 */
function momentAt(doc: Text, line: number, now?: DateTime) {
  const { timestamp } = eventAt(doc, line)
  if (timestamp) {
    return DateTime.fromISO(timestamp)
  }
  return now || DateTime.now()
}

/**
 * Return the tracking file content.
 * The file is only read once regardless of how many times this is called.
 */
async function getContent() {
  if (
    dirty ||
    typeof fileString !== "string" ||
    typeof editor === "undefined"
  ) {
    try {
      fileString = await readFile(trackingFile(), { encoding: "utf-8" })
    } catch (e) {
      if ((e as ErrnoException).code === "ENOENT") {
        await mkdir(dirname(trackingFile()), { recursive: true })
        await appendFile(trackingFile(), "", { encoding: "utf-8" })
        // create the file
        fileString = ""
      } else {
        throw e
      }
    }
    editor = EditorState.create({ doc: fileString })
  }
  dirty = false
  return editor
}

/** Return the line for the moment in the file. */
export async function getMomentLine(moment: DateTime) {
  const doc = (await getContent()).doc
  let high = doc.lines - 1 // we do 0-based indexing here
  let low = 0
  let mid = Math.floor((high + low) / 2)

  while (high > low) {
    mid = Math.floor((high + low) / 2)
    const { timestamp } = eventAt(doc, mid)
    if (timestamp) {
      const eventMoment = DateTime.fromISO(timestamp)
      if (moment < eventMoment) {
        high = mid - 1
      } else if (eventMoment < moment) {
        low = mid + 1
      } else {
        break
      }
    } else {
      // nudge `high` back if we can't grab the event
      high--
    }
  }
  // nudge forward to the right line
  while (moment >= momentAt(doc, mid) && mid < doc.lines) {
    mid++
  }
  // We've moved past the boundary to see the boundary. Move back.
  mid--
  return mid
}

/**
 * Return raw events as an iterator.
 * @param dir - direction, 1 for old to new, -1 for new to old
 */
async function getEvents(dir: 1 | -1 = 1) {
  const editor = await getContent()
  const lineBreak = editor.lineBreak
  return iter(editor.doc.iter(dir))
    .filter((line) => line !== lineBreak)
    .map(parseEvent)
    .filter((event) => {
      return event.timestamp !== null
    }) as HIterator<Event<string>>
}

export async function GetMomentTask(moment: DateTime) {
  const doc = (await getContent()).doc
  const event = eventAt(doc, await getMomentLine(moment))
  return event.task
}

/**
 * Return all task names, sorted from most recently active to least.
 */
export async function GetTaskNames() {
  const tasks: Set<string> = new Set()
  for (const { task } of await getEvents(-1)) {
    tasks.add(task)
    for (const ancestor of taskAncestors(task)) {
      tasks.add(ancestor)
    }
  }
  return tasks
}

// export async function GetCurrentTask() {
//   import { createFileReadStream } from "../../file-web-streams/";
//   const iter = createFileReadStream(trackingFile()).values();
//   let event = await iter.next();
//   while (event.value === "\n") {
//     event = await iter.next();
//   }
//   return event.value
// }
export async function GetCurrentTask() {
  const last = [...(await getEvents(-1)).take(1)][0]
  if (last?.task) return last.task
}

/**
 * Return the number of milliseconds `task` has been running for.
 * If `descendants` is true (default), include descendants.
 */
export async function GetTaskDuration(task: string, descendants = true) {
  /** Temporary area for dates. */
  let keepNext = false
  let start: DateTime | undefined
  let duration = 0
  let lastActiveTimestamp = ""
  for (const { timestamp, task: filetask } of await getEvents()) {
    if (filetask === task || (descendants && filetask.startsWith(task + ":"))) {
      if (keepNext) continue
      start = DateTime.fromISO(timestamp)
      keepNext = true
    } else {
      if (!keepNext) continue
      const end = DateTime.fromISO(timestamp)
      // We can only reach here if `keepNext` is not false, and `keepNext` is
      // only ever not false if we have ever set `start`, so yes, we know better
      // than TypeScript in this case, that using `start` here is fine.
      duration += end.toMillis() - start!.toMillis()
      lastActiveTimestamp = timestamp
      keepNext = false
    }
  }
  return { duration, lastActiveTimestamp }
}

export async function SwitchTask(task: string, moment?: DateTime) {
  if (task === (await GetCurrentTask())) {
    return false
  }
  const timestamp = timeToString(moment)
  const handler = await open(trackingFile(), "a")
  handler.appendFile( `${timestamp}  ${task}\n`)
  dirty = true
  return true
}

/**
 * Handle merge conflicts, preserving every event.
 */
export async function EditDoMerge() {
  const editor = await getContent()
  const lineBreak = editor.lineBreak
  const lines: string[] = []
  for (const line of editor.doc.iter()) {
    if (
      line === lineBreak ||
      line.startsWith("|||") ||
      line.startsWith("===") ||
      line.startsWith(">>>")
    ) {
      continue
    }
    lines.push(line)
  }
  await writeFile(trackingFile(), lines.join(lineBreak) + lineBreak)
}
