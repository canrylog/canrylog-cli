/**
 * @file Configuration.
 * Try to avoid accessing `config` directly if there are seemingly equivalent
 * getter or setter functions.
 */

import { homedir } from "node:os"
import * as path from "node:path"
import Conf from "conf"

import pkg from "../package.json"
import { tryOrExit } from "./utils"

let _config: Conf<{ "path.repo": unknown; "sync.enabled": unknown }> | undefined
export function config() {
  if (typeof _config !== "undefined") return _config
  _config = tryOrExit(() => {
    return new Conf({
      projectName: pkg.name,
      projectSuffix: "",
      accessPropertiesByDotNotation: false,
      schema: {
        "path.repo": {
          type: "string",
        },
        "sync.enabled": {
          type: "boolean",
        },
      },
      defaults: {
        "path.repo": `${homedir()}/.canrylog`,
        "sync.enabled": true,
      },
    })
  })
  return _config
}

/**
 * Return the full path to the tracking file.
 */
let _file: string | undefined
export function trackingFile(): string {
  if (typeof _file === "string") return _file
  const repo = config().get("path.repo")
  const file = "tracking.canrylog"
  if (typeof repo !== "string" || typeof file !== "string") {
    throw new Error("Configuration file is not valid")
  }
  _file = path.join(repo, file)
  return _file
}
