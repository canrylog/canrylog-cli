import { DateTime, Duration } from "luxon"
import { Err, Ok } from "ts-results-es"

import { spawnSync } from "node:child_process"
import type { GitError } from "simple-git"
import { config, trackingFile } from "./config.ts"
import { EditDoMerge, GetTaskDuration, SwitchTask } from "./file.ts"
import { repoShouldSync } from "./git.ts"
import { substituteChalkTemplate } from "./logging.ts"
import { ReadTask } from "./read.ts"
import { promiseResult } from "./utils.ts"

export function cmdConfig(maybeKey?: string, value?: string) {
  const options = Object.keys(config().store)
  if (typeof maybeKey !== "string" || !options.includes(maybeKey)) {
    console.log("Options:")
    for (const opt of options) {
      console.log(`  ${opt}`)
    }
  } else if (typeof value !== "string") {
    console.log(config().get(maybeKey))
  } else {
    // HACK: allow setting boolean value
    if (value === "true") {
      config().set(maybeKey, true)
    } else if (value === "false") {
      config().set(maybeKey, false)
    } else {
      config().set(maybeKey, value)
    }
  }
}

export async function cmdSwitchTask(maybeTask: unknown) {
  // evolving any
  let result
  const task = maybeTask || (await ReadTask("Switch to task"))
  if (typeof task !== "string") {
    console.error("The task should be a string")
    return
  }
  console.log("Pulling...")
  result = await cmdSyncPullRebase(true)
  if (result.isErr() && result.error !== "should not sync") {
    console.log("Pull failed, continuing")
  }

  console.log(`Switching to ${task}...`)
  result = await SwitchTask(task, DateTime.now())
  if (!result) {
    console.log(`Current task is already ${task}.`)
    return
  }
  console.log(`Switching to ${task}...done`)

  console.log("Pushing...")
  result = await cmdSyncPush(true)
  if (result.isErr() && result.error !== "should not sync") {
    console.log("Push failed")
  } else {
    console.log("Push successful")
  }
}

export async function cmdTaskInfo(maybeTask: unknown) {
  const task = maybeTask || (await ReadTask("Show info for task"))
  if (typeof task !== "string") {
    console.error("The task should be a string")
    return
  }
  // TODO: configuration for how durations are shown
  const { duration, lastActiveTimestamp } = await GetTaskDuration(task)
  console.log(
    substituteChalkTemplate(
      `Task: {bold ${task}}

Last active: ${lastActiveTimestamp}
Total duration: ${Duration.fromMillis(duration).toFormat("hh:mm:ss")}`,
    ),
  )
}

/**
 * Do a push.
 * Return whether it was successful.
 */
export async function cmdSyncPush(silent = false) {
  const repo = await repoShouldSync()
  if (!repo) return Err("should not sync" as const)
  if (!silent) console.log("Pushing...")
  const ret = await promiseResult(
    repo.add(trackingFile()).commit("auto sync").push(),
  )
  if (ret.isErr()) {
    if (!silent) console.log(ret.error.message)
  }
  return ret
}
/**
 * Do a fetch and rebase.
 * Return whether it was successful.
 */
export async function cmdSyncPullRebase(silent = false) {
  const repo = await repoShouldSync()
  if (!repo) return Err("should not sync" as const)
  if (!silent) console.log("Pulling...")
  const fetchResult = await promiseResult(repo.fetch())
  if (fetchResult.isErr()) {
    return Err("fetch failed" as const)
  }
  const rebaseResult = await promiseResult(repo.rebase())
  if (rebaseResult.isErr()) {
    try {
      console.log("There are conflicts. Auto resolving...")
      // merge conflict; resolve it and continue
      await EditDoMerge()
      console.log("Marking as resolved...")
      await repo.add(trackingFile())
      console.log("Continuing...")
      await repo
        // biome-ignore lint: external API
        .env({ ...process.env, GIT_SEQUENCE_EDITOR: ":", GIT_EDITOR: ":" })
        .rebase(["--continue"])
        .catch((e: GitError) => {
          console.log(e.message)
        })
    } catch {
      return Err("rebase failed" as const)
    }
  }
  return Ok(true)
}
