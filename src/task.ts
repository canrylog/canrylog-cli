/**
 * Functions for dealing with task names.
 */

/**
 * Return a list of ancestors of `task`, sorted from longest to shortest.
 */
export function taskAncestors(task: string): string[] {
  const matches: number[] = []
  let i = 0
  for (const c of task) {
    if (c === ":") matches.push(i)
    i++
  }
  return matches.map((i) => task.substring(0, i)).reverse()
}

/** Return the basename of `task`. */
export function taskBasename(task: string): string {
  return task.replace(/^.*:/, "")
}
