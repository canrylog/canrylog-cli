# canrylog-cli
[![License: GPL v3+](https://img.shields.io/badge/License-GPL%20v3+-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![NPM Version](https://img.shields.io/npm/v/canrylog)](https://npmjs.com/package/canrylog)

CLI version of Canrylog, another time tracking system.

Emacs version: <https://gitlab.com/canrylog/canrylog.el>

My take on time tracking.

Currently in experimental stage, developed with the assumption that I'm the only user.

## Getting started

- Install the package
- By default, the tracking file will be put under `~/.canrylog`. To change this, modify the `path.repo` configuration option.
- `canrylog switch <task>` to start tracking time
- `canrylog switch Downtime` to switch to the task called “Downtime”, which by convention means clocked out.
- `canrylog visit` to edit the tracking file with your editor. Hand editing is error prone: make sure events are sorted correctly and the timestamps are in the right format. Each event is `<timestamp>  <task name>` (two spaces between).

## Technology stack

- Using Bun as the package manager and bundler. It's not that Bun has great DX compared to, say, Cargo, it's just that Node and TypeScript's DX is infuriating. Let me import with the normal syntax in your repl! Why does the TS language server not work without a project wide config file?! Please use hard links or symlinks to save space usage!
- Package distributed [to npm](https://npmjs.com/package/canrylog)
- argument handling done with commander
- Prompts with enquirer
- Manages configuration with conf

## Install

```sh
npm install -g canrylog
```

## Use

- [X] `canrylog about`: print an about message for Canrylog
- [X] `canrylog help [command]`: print help text, optionally for `[command]`

- [X] `canrylog config`: see all configuration keys. TODO: make this interactive
- [X] `canrylog config <key>`: read value of configuration `<key>`
- [X] `canrylog config <key> <value>`: set configuration `<key>` to `<value>`

- [X] `canrylog switch`: select a task and switch to it
- [X] `canrylog switch <task>`: switch to `<task>` at the current moment
- [ ] `canrylog switch <task> <moment>`: switch to `<task>` at the specified moment
- [X] `canrylog clock-out`: Same as switching to the “Downtime” task, representing the state of not doing anything of note.

- [X] `canrylog status`: print current task and status
- [X] `canrylog task <task>`: print information about `<task>`
- [X] `canrylog moment <moment>`: print information about `<moment>`

Git-based tracking file synchonization:

- [X] `sync.enabled` config option. If true and the tracking folder is a Git repository, then automatic synchronization will be performed after switching tasks.
- [X] `canrylog sync`: pull then push
- [X] `canrylog sync pull`: pull down new changes. Actually more like `git fetch; git rebase`.
- [X] `canrylog sync push`: push changes to Git remote

Implemented for debugging but might be useful in the future:

- [X] `canrylog list-tasks`: print all available tasks
- [X] `canrylog visit`: edit the tracking file with `$EDITOR`
- [ ] `canrylog rename-task <task> <newname>`: rename `<task>` to `<newname>`


## Configuration

Incomplete.

# Other solutions

<https://www.emacswiki.org/emacs/CategoryTimeTracking>

- [logtime](https://github.com/bevesce/logtime): plain text, very similar in concept
- `timeclock.el`: <kbd>C-h f</kbd> `timeclock-in` <kbd>RET</kbd>
- Org mode
- [WakaTime](https://wakatime.com/), `wakatime-mode` on MELPA
- [ActivityWatch](https://activitywatch.net), `activity-watch-mode` on MELPA
- [harvestime](https://github.com/jgdavey/harvestime): plain text as well, centered around vim
